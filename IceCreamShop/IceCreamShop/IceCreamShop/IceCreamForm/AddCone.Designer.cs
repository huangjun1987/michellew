﻿namespace IceCreamForm
{
    partial class AddCone
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.numScoops)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxFlavor
            // 
            this.comboBoxFlavor.Size = new System.Drawing.Size(316, 39);
            // 
            // comboBoxConeType
            // 
            this.comboBoxConeType.Size = new System.Drawing.Size(316, 39);
            // 
            // comboBoxChoice
            // 
            this.comboBoxChoice.Size = new System.Drawing.Size(316, 39);
            // 
            // buttonOK
            // 
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // AddCone
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.ClientSize = new System.Drawing.Size(1253, 671);
            this.Name = "AddCone";
            this.Text = "Add Cone";
            ((System.ComponentModel.ISupportInitialize)(this.numScoops)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
