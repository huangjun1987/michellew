﻿namespace IceCreamForm
{
    partial class DisplayOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.labelCustomerName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.listViewCones = new System.Windows.Forms.ListView();
            this.buttonDeleteCone = new System.Windows.Forms.Button();
            this.buttonViewCustomers = new System.Windows.Forms.Button();
            this.buttonEditCone = new System.Windows.Forms.Button();
            this.buttonAddCone = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(234, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Customer Name: ";
            // 
            // labelCustomerName
            // 
            this.labelCustomerName.AutoSize = true;
            this.labelCustomerName.Location = new System.Drawing.Point(311, 13);
            this.labelCustomerName.Name = "labelCustomerName";
            this.labelCustomerName.Size = new System.Drawing.Size(0, 32);
            this.labelCustomerName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(47, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 32);
            this.label2.TabIndex = 2;
            this.label2.Text = "Total:";
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(217, 66);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(0, 32);
            this.labelTotal.TabIndex = 3;
            // 
            // listViewCones
            // 
            this.listViewCones.FullRowSelect = true;
            this.listViewCones.HideSelection = false;
            this.listViewCones.Location = new System.Drawing.Point(53, 117);
            this.listViewCones.Name = "listViewCones";
            this.listViewCones.Size = new System.Drawing.Size(1066, 446);
            this.listViewCones.TabIndex = 4;
            this.listViewCones.UseCompatibleStateImageBehavior = false;
            // 
            // buttonDeleteCone
            // 
            this.buttonDeleteCone.Location = new System.Drawing.Point(581, 581);
            this.buttonDeleteCone.Name = "buttonDeleteCone";
            this.buttonDeleteCone.Size = new System.Drawing.Size(265, 71);
            this.buttonDeleteCone.TabIndex = 8;
            this.buttonDeleteCone.Text = "Delete Cone";
            this.buttonDeleteCone.UseVisualStyleBackColor = true;
            this.buttonDeleteCone.Click += new System.EventHandler(this.buttonDeleteCone_Click);
            // 
            // buttonViewCustomers
            // 
            this.buttonViewCustomers.Location = new System.Drawing.Point(872, 581);
            this.buttonViewCustomers.Name = "buttonViewCustomers";
            this.buttonViewCustomers.Size = new System.Drawing.Size(247, 71);
            this.buttonViewCustomers.TabIndex = 7;
            this.buttonViewCustomers.Text = "View Customers";
            this.buttonViewCustomers.UseVisualStyleBackColor = true;
            this.buttonViewCustomers.Click += new System.EventHandler(this.buttonViewCustomers_Click);
            // 
            // buttonEditCone
            // 
            this.buttonEditCone.Location = new System.Drawing.Point(323, 581);
            this.buttonEditCone.Name = "buttonEditCone";
            this.buttonEditCone.Size = new System.Drawing.Size(232, 71);
            this.buttonEditCone.TabIndex = 6;
            this.buttonEditCone.Text = "Edit Cone";
            this.buttonEditCone.UseVisualStyleBackColor = true;
            this.buttonEditCone.Click += new System.EventHandler(this.buttonEditCone_Click);
            // 
            // buttonAddCone
            // 
            this.buttonAddCone.Location = new System.Drawing.Point(53, 581);
            this.buttonAddCone.Name = "buttonAddCone";
            this.buttonAddCone.Size = new System.Drawing.Size(233, 65);
            this.buttonAddCone.TabIndex = 5;
            this.buttonAddCone.Text = "Add Cone";
            this.buttonAddCone.UseVisualStyleBackColor = true;
            this.buttonAddCone.Click += new System.EventHandler(this.buttonAddCone_Click);
            // 
            // DisplayOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1419, 658);
            this.Controls.Add(this.buttonDeleteCone);
            this.Controls.Add(this.buttonViewCustomers);
            this.Controls.Add(this.buttonEditCone);
            this.Controls.Add(this.buttonAddCone);
            this.Controls.Add(this.listViewCones);
            this.Controls.Add(this.labelTotal);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelCustomerName);
            this.Controls.Add(this.label1);
            this.Name = "DisplayOrder";
            this.Text = "DisplayOrder";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label labelCustomerName;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label labelTotal;
        public System.Windows.Forms.ListView listViewCones;
        public System.Windows.Forms.Button buttonDeleteCone;
        public System.Windows.Forms.Button buttonViewCustomers;
        public System.Windows.Forms.Button buttonEditCone;
        public System.Windows.Forms.Button buttonAddCone;
    }
}