﻿using IceCreamForm.Models;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace IceCreamForm
{
    public partial class Form1 : Form
    {
        Customer currentCustomer;
        private DataRepository dataRepository = new DataRepository();
        Order currentOrder;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadChoices();

            LoadAllCustomers();
        }

        private void LoadChoices()
        {
            cmbIcecreamType.Items.Clear();
            cmbIcecreamType.Items.AddRange(dataRepository.Choices.ToArray());
            cmbIcecreamType.DisplayMember = "Name";
            cmbIcecreamType.ValueMember = "Id";
        }

        private void LoadAllCustomers()
        {
            lstCustomers.Items.Clear();
            lstCustomers.DisplayMember = "FullName";
            lstCustomers.ValueMember = "CustomerID";
            lstCustomers.Items.AddRange(dataRepository.Customers.ToArray());
            lstCustomers.SelectedIndex = 0;
        }

        private void lstCustomers_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentCustomer = (Customer)lstCustomers.SelectedItem;
            LoadCurrentOrder();
        }

        private void LoadCurrentOrder()
        {
            cmbIcecreamType.SelectedIndex = 0;
            comboBoxFlavor.SelectedIndex = 0;
            comboBoxConeType.SelectedIndex = 0;
            numScoops.Value = 1;
            currentOrder = dataRepository.GetOrderByCustomerId(currentCustomer.CustomerID);
            lstOrder.Items.Clear();
            lstOrder.Items.AddRange(currentOrder.Cones.Where(c => c != null).ToArray());
            this.Update();
            labelTotal.Text = dataRepository.GetTotal(currentCustomer.CustomerID);
        }

        private void btnAddCustomer_Click(object sender, EventArgs e)
        {
            try
            {
                var firstName = txtFName.Text;
                var lastName = txtLName.Text;
                dataRepository.AddCustomer(lastName, firstName, DisplayMessage);
                LoadAllCustomers();
                txtFName.Clear();
                txtLName.Clear();
            }
            catch (Exception exception)
            {
                MessageBox.Show($"Failed to add customer: {exception.Message}");
            }
        }

        private void btnDeleteCustomer_Click(object sender, EventArgs e)
        {
            try
            {
                currentCustomer = (Customer)lstCustomers.SelectedItem;
                if (currentCustomer != null)
                {
                    dataRepository.DeleteCustomer(currentCustomer.CustomerID, DisplayMessage);
                    LoadAllCustomers();
                    LoadCurrentOrder();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Failed to delete customer: {ex.Message}");
            }


        }

        private void btnAddCone_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedIcecreamType = (Choice)cmbIcecreamType.SelectedItem?? dataRepository.Choices[0];
                dataRepository.AddCone(currentCustomer.CustomerID, selectedIcecreamType.Id, comboBoxFlavor.SelectedItem.ToString(), (int)numScoops.Value, comboBoxConeType.SelectedItem.ToString(), DisplayMessage);                
                LoadCurrentOrder();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Failed to add cone: {ex.Message}");
            }

        }

        private void btnDeleteCone_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedCone = (Cone)lstOrder.SelectedItem;
                dataRepository.DeleteCone(currentCustomer.CustomerID, selectedCone.ConeId, DisplayMessage);
                LoadCurrentOrder();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Failed to delete cone: {ex.Message}");
            }
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public void DisplayMessage(string message)
        {
            MessageBox.Show(message);
        }
    }
}
