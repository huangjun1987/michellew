﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace IceCreamForm
{
    public partial class AddCustomer : IceCreamForm.ParentForm
    {
        private DataRepository dataRepository = new DataRepository();

        public AddCustomer()
        {
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            try
            {
                var firstName = txtFName.Text;
                var lastName = txtLName.Text;
                dataRepository.AddCustomer(lastName, firstName, DelegateHelper.DisplayMessage);               
                txtFName.Clear();
                txtLName.Clear();
                RedirectToListCustomers();
            }
            catch (Exception exception)
            {
                MessageBox.Show($"Failed to add customer: {exception.Message}");
            }
        }
    }
}
