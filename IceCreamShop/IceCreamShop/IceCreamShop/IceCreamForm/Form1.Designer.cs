﻿using IceCreamForm.Models;

namespace IceCreamForm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnAddCustomer = new System.Windows.Forms.Button();
            this.btnDeleteCustomer = new System.Windows.Forms.Button();
            this.lstCustomers = new System.Windows.Forms.ListBox();
            this.lstOrder = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFName = new System.Windows.Forms.TextBox();
            this.txtLName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbIcecreamType = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.F = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.numScoops = new System.Windows.Forms.NumericUpDown();
            this.btnAddCone = new System.Windows.Forms.Button();
            this.btnDeleteCone = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxConeType = new System.Windows.Forms.ComboBox();
            this.comboBoxFlavor = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.customerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.numScoops)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.customerBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAddCustomer
            // 
            this.btnAddCustomer.Location = new System.Drawing.Point(203, 279);
            this.btnAddCustomer.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnAddCustomer.Name = "btnAddCustomer";
            this.btnAddCustomer.Size = new System.Drawing.Size(285, 55);
            this.btnAddCustomer.TabIndex = 0;
            this.btnAddCustomer.Text = "Add Customer";
            this.btnAddCustomer.UseVisualStyleBackColor = true;
            this.btnAddCustomer.Click += new System.EventHandler(this.btnAddCustomer_Click);
            // 
            // btnDeleteCustomer
            // 
            this.btnDeleteCustomer.Location = new System.Drawing.Point(539, 589);
            this.btnDeleteCustomer.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnDeleteCustomer.Name = "btnDeleteCustomer";
            this.btnDeleteCustomer.Size = new System.Drawing.Size(285, 55);
            this.btnDeleteCustomer.TabIndex = 1;
            this.btnDeleteCustomer.Text = "Delete Customer";
            this.btnDeleteCustomer.UseVisualStyleBackColor = true;
            this.btnDeleteCustomer.Click += new System.EventHandler(this.btnDeleteCustomer_Click);
            // 
            // lstCustomers
            // 
            this.lstCustomers.FormattingEnabled = true;
            this.lstCustomers.ItemHeight = 31;
            this.lstCustomers.Location = new System.Drawing.Point(534, 100);
            this.lstCustomers.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.lstCustomers.Name = "lstCustomers";
            this.lstCustomers.Size = new System.Drawing.Size(295, 469);
            this.lstCustomers.TabIndex = 2;
            this.lstCustomers.SelectedIndexChanged += new System.EventHandler(this.lstCustomers_SelectedIndexChanged);
            // 
            // lstOrder
            // 
            this.lstOrder.FormattingEnabled = true;
            this.lstOrder.ItemHeight = 31;
            this.lstOrder.Location = new System.Drawing.Point(1445, 105);
            this.lstOrder.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.lstOrder.Name = "lstOrder";
            this.lstOrder.Size = new System.Drawing.Size(1162, 469);
            this.lstOrder.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 105);
            this.label1.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 32);
            this.label1.TabIndex = 4;
            this.label1.Text = "First Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 184);
            this.label2.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 32);
            this.label2.TabIndex = 5;
            this.label2.Text = "Last Name";
            // 
            // txtFName
            // 
            this.txtFName.Location = new System.Drawing.Point(203, 105);
            this.txtFName.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.txtFName.Name = "txtFName";
            this.txtFName.Size = new System.Drawing.Size(279, 38);
            this.txtFName.TabIndex = 6;
            // 
            // txtLName
            // 
            this.txtLName.Location = new System.Drawing.Point(203, 184);
            this.txtLName.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.txtLName.Name = "txtLName";
            this.txtLName.Size = new System.Drawing.Size(279, 38);
            this.txtLName.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(195, 29);
            this.label3.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(271, 32);
            this.label3.TabIndex = 8;
            this.label3.Text = "Add a new customer";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(646, 21);
            this.label4.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(218, 32);
            this.label4.TabIndex = 9;
            this.label4.Text = "Select customer";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1676, 21);
            this.label5.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(182, 32);
            this.label5.TabIndex = 10;
            this.label5.Text = "Current order";
            // 
            // cmbIcecreamType
            // 
            this.cmbIcecreamType.FormattingEnabled = true;
            this.cmbIcecreamType.Items.AddRange(new object[] {
            "Ice Cream",
            "Frozen Yogurt"});
            this.cmbIcecreamType.Location = new System.Drawing.Point(1075, 100);
            this.cmbIcecreamType.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.cmbIcecreamType.Name = "cmbIcecreamType";
            this.cmbIcecreamType.Size = new System.Drawing.Size(316, 39);
            this.cmbIcecreamType.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(861, 105);
            this.label6.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(200, 32);
            this.label6.TabIndex = 12;
            this.label6.Text = "Icecream Type";
            // 
            // F
            // 
            this.F.AutoSize = true;
            this.F.Location = new System.Drawing.Point(901, 184);
            this.F.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.F.Name = "F";
            this.F.Size = new System.Drawing.Size(94, 32);
            this.F.TabIndex = 14;
            this.F.Text = "Flavor";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(909, 255);
            this.label7.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 32);
            this.label7.TabIndex = 15;
            this.label7.Text = "Scoops";
            // 
            // numScoops
            // 
            this.numScoops.Location = new System.Drawing.Point(1075, 255);
            this.numScoops.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.numScoops.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numScoops.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numScoops.Name = "numScoops";
            this.numScoops.Size = new System.Drawing.Size(320, 38);
            this.numScoops.TabIndex = 16;
            this.numScoops.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnAddCone
            // 
            this.btnAddCone.Location = new System.Drawing.Point(1075, 439);
            this.btnAddCone.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnAddCone.Name = "btnAddCone";
            this.btnAddCone.Size = new System.Drawing.Size(285, 55);
            this.btnAddCone.TabIndex = 17;
            this.btnAddCone.Text = "Add Cone";
            this.btnAddCone.UseVisualStyleBackColor = true;
            this.btnAddCone.Click += new System.EventHandler(this.btnAddCone_Click);
            // 
            // btnDeleteCone
            // 
            this.btnDeleteCone.Location = new System.Drawing.Point(1709, 594);
            this.btnDeleteCone.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnDeleteCone.Name = "btnDeleteCone";
            this.btnDeleteCone.Size = new System.Drawing.Size(285, 55);
            this.btnDeleteCone.TabIndex = 18;
            this.btnDeleteCone.Text = "Delete Cone";
            this.btnDeleteCone.UseVisualStyleBackColor = true;
            this.btnDeleteCone.Click += new System.EventHandler(this.btnDeleteCone_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(1957, 17);
            this.buttonExit.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(200, 55);
            this.buttonExit.TabIndex = 19;
            this.buttonExit.Text = "Close";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(901, 355);
            this.label8.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(153, 32);
            this.label8.TabIndex = 21;
            this.label8.Text = "Cone Type";
            // 
            // comboBoxConeType
            // 
            this.comboBoxConeType.FormattingEnabled = true;
            this.comboBoxConeType.Items.AddRange(new object[] {
            "Waffle",
            "Cake",
            "Sugar"});
            this.comboBoxConeType.Location = new System.Drawing.Point(1075, 351);
            this.comboBoxConeType.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.comboBoxConeType.Name = "comboBoxConeType";
            this.comboBoxConeType.Size = new System.Drawing.Size(316, 39);
            this.comboBoxConeType.TabIndex = 20;
            // 
            // comboBoxFlavor
            // 
            this.comboBoxFlavor.FormattingEnabled = true;
            this.comboBoxFlavor.Items.AddRange(new object[] {
            "Vanilla",
            "Cream of Cocoa",
            "Chocolate Chip",
            "Cherry"});
            this.comboBoxFlavor.Location = new System.Drawing.Point(1075, 184);
            this.comboBoxFlavor.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.comboBoxFlavor.Name = "comboBoxFlavor";
            this.comboBoxFlavor.Size = new System.Drawing.Size(316, 39);
            this.comboBoxFlavor.TabIndex = 22;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1445, 63);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 32);
            this.label9.TabIndex = 23;
            this.label9.Text = "Total:";
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(1566, 63);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(0, 32);
            this.labelTotal.TabIndex = 24;
            // 
            // customerBindingSource
            // 
            this.customerBindingSource.DataSource = typeof(Customer);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2624, 769);
            this.Controls.Add(this.labelTotal);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.comboBoxFlavor);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.comboBoxConeType);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.btnDeleteCone);
            this.Controls.Add(this.btnAddCone);
            this.Controls.Add(this.numScoops);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.F);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cmbIcecreamType);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtLName);
            this.Controls.Add(this.txtFName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lstOrder);
            this.Controls.Add(this.lstCustomers);
            this.Controls.Add(this.btnDeleteCustomer);
            this.Controls.Add(this.btnAddCustomer);
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numScoops)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.customerBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddCustomer;
        private System.Windows.Forms.Button btnDeleteCustomer;
        private System.Windows.Forms.ListBox lstCustomers;
        private System.Windows.Forms.ListBox lstOrder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFName;
        private System.Windows.Forms.TextBox txtLName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbIcecreamType;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.BindingSource customerBindingSource;
        private System.Windows.Forms.Label F;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numScoops;
        private System.Windows.Forms.Button btnAddCone;
        private System.Windows.Forms.Button btnDeleteCone;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBoxConeType;
        private System.Windows.Forms.ComboBox comboBoxFlavor;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelTotal;
    }
}

