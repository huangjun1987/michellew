﻿namespace IceCreamForm
{
    partial class ParentFormOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxFlavor = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxConeType = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.numScoops = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.F = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxChoice = new System.Windows.Forms.ComboBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.labelCustomerName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numScoops)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxFlavor
            // 
            this.comboBoxFlavor.FormattingEnabled = true;
            this.comboBoxFlavor.Items.AddRange(new object[] {
            "Vanilla",
            "Cream of Cocoa",
            "Chocolate Chip",
            "Cherry"});
            this.comboBoxFlavor.Location = new System.Drawing.Point(347, 181);
            this.comboBoxFlavor.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.comboBoxFlavor.Name = "comboBoxFlavor";
            this.comboBoxFlavor.Size = new System.Drawing.Size(316, 39);
            this.comboBoxFlavor.TabIndex = 31;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(173, 352);
            this.label8.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(153, 32);
            this.label8.TabIndex = 30;
            this.label8.Text = "Cone Type";
            // 
            // comboBoxConeType
            // 
            this.comboBoxConeType.FormattingEnabled = true;
            this.comboBoxConeType.Items.AddRange(new object[] {
            "Waffle",
            "Cake",
            "Sugar"});
            this.comboBoxConeType.Location = new System.Drawing.Point(347, 348);
            this.comboBoxConeType.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.comboBoxConeType.Name = "comboBoxConeType";
            this.comboBoxConeType.Size = new System.Drawing.Size(316, 39);
            this.comboBoxConeType.TabIndex = 29;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(347, 436);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(285, 55);
            this.btnCancel.TabIndex = 28;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // numScoops
            // 
            this.numScoops.Location = new System.Drawing.Point(347, 252);
            this.numScoops.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.numScoops.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numScoops.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numScoops.Name = "numScoops";
            this.numScoops.Size = new System.Drawing.Size(320, 38);
            this.numScoops.TabIndex = 27;
            this.numScoops.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(181, 252);
            this.label7.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 32);
            this.label7.TabIndex = 26;
            this.label7.Text = "Scoops";
            // 
            // F
            // 
            this.F.AutoSize = true;
            this.F.Location = new System.Drawing.Point(173, 181);
            this.F.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.F.Name = "F";
            this.F.Size = new System.Drawing.Size(94, 32);
            this.F.TabIndex = 25;
            this.F.Text = "Flavor";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(133, 102);
            this.label6.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 32);
            this.label6.TabIndex = 24;
            this.label6.Text = "Choice";
            // 
            // comboBoxChoice
            // 
            this.comboBoxChoice.FormattingEnabled = true;
            this.comboBoxChoice.Items.AddRange(new object[] {
            "Ice Cream",
            "Frozen Yogurt"});
            this.comboBoxChoice.Location = new System.Drawing.Point(347, 97);
            this.comboBoxChoice.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.comboBoxChoice.Name = "comboBoxChoice";
            this.comboBoxChoice.Size = new System.Drawing.Size(316, 39);
            this.comboBoxChoice.TabIndex = 23;
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(30, 436);
            this.buttonOK.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(285, 55);
            this.buttonOK.TabIndex = 32;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            // 
            // labelCustomerName
            // 
            this.labelCustomerName.AutoSize = true;
            this.labelCustomerName.Location = new System.Drawing.Point(403, 36);
            this.labelCustomerName.Name = "labelCustomerName";
            this.labelCustomerName.Size = new System.Drawing.Size(0, 32);
            this.labelCustomerName.TabIndex = 34;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(133, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(234, 32);
            this.label1.TabIndex = 33;
            this.label1.Text = "Customer Name: ";
            // 
            // ParentFormOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1253, 671);
            this.Controls.Add(this.labelCustomerName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.comboBoxFlavor);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.comboBoxConeType);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.numScoops);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.F);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.comboBoxChoice);
            this.Name = "ParentFormOrder";
            this.Text = "ParentFormOrder";
            ((System.ComponentModel.ISupportInitialize)(this.numScoops)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ComboBox comboBoxFlavor;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.ComboBox comboBoxConeType;
        public System.Windows.Forms.Button btnCancel;
        public System.Windows.Forms.NumericUpDown numScoops;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label F;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.ComboBox comboBoxChoice;
        public System.Windows.Forms.Button buttonOK;
        public System.Windows.Forms.Label labelCustomerName;
        public System.Windows.Forms.Label label1;
    }
}