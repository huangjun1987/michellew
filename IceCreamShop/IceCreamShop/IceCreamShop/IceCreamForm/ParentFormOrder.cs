﻿using IceCreamForm.Models;
using System;
using System.Windows.Forms;

namespace IceCreamForm
{
    public partial class ParentFormOrder : Form
    {
        public Order currentOrder;
        public DataRepository dataRepository = new DataRepository();

        public ParentFormOrder()
        {
            InitializeComponent();
        }
        public ParentFormOrder(Order order)
        {
            currentOrder = order;
            InitializeComponent();
            labelCustomerName.Text = $"{currentOrder.Customer.FName} {currentOrder.Customer.LName}";
            LoadChoices(dataRepository.Choices.ToArray());
            LoadFlavors();
            LoadConeTypes();
        }

        private void LoadConeTypes()
        {
            var coneTypes = new string[] { "Waffle","Cake","Sugar" };
            comboBoxConeType.Items.Clear();
            comboBoxConeType.DataSource = coneTypes;
        }

        private void LoadFlavors()
        {
            var flavors = new string[] { "Vanilla", "Cream of Cocoa", "Chocolate Chip", "Cherry" };
            comboBoxFlavor.Items.Clear();
            comboBoxFlavor.DataSource = flavors;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
            var form = new DisplayOrder(currentOrder.Customer.CustomerID);
            form.Show();
        }

        public void LoadChoices(Choice[] choices)
        {
            comboBoxChoice.Items.Clear();
            comboBoxChoice.DataSource = choices;
            comboBoxChoice.DisplayMember = "Name";
            comboBoxChoice.ValueMember = "Id";
        }
    }
}
