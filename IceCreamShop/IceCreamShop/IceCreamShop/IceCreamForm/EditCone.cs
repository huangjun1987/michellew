﻿using IceCreamForm.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace IceCreamForm
{
    public partial class EditCone : IceCreamForm.ParentFormOrder
    {
        private Cone currentCone;
        public EditCone(Order order, int coneId) : base(order)
        {
            InitializeComponent();
            currentCone = GetConeByConeId(order, coneId);
            if (currentCone != null)
            {
                comboBoxChoice.SelectedIndex = currentCone.GetChoiceId() == 1 ? 0 : 1;
                comboBoxFlavor.SelectedItem = currentCone.Flavor;
                comboBoxConeType.SelectedItem = currentCone.ConeType;
                numScoops.Value = currentCone.Scoops;
            }
        }

        private Cone GetConeByConeId(Order order, int coneId)
        {
            for (int i = 0; i < order.Cones.Length; i++)
            {
                if (order.Cones[i].ConeId == coneId)
                {
                    return order.Cones[i];
                }
            }
            return null;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedIcecreamType = (Choice)comboBoxChoice.SelectedItem ?? dataRepository.Choices[0];
                var updatedCone = ConeFactory.GetCone(selectedIcecreamType.Id);
                updatedCone.ConeId = currentCone.ConeId;
                updatedCone.CustomerId = currentOrder.Customer.CustomerID;
                updatedCone.ConeType = comboBoxConeType.SelectedItem.ToString();
                updatedCone.Flavor = comboBoxFlavor.SelectedItem.ToString();
                updatedCone.Scoops = (int)numScoops.Value;
                dataRepository.EditCone(currentOrder, updatedCone, DelegateHelper.DisplayMessage);
                this.Hide();
                var form = new DisplayOrder(currentOrder.Customer.CustomerID);
                form.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Failed to edit cone: {ex.Message}");
            }
        }
    }
}
