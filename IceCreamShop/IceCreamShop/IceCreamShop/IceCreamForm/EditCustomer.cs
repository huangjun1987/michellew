﻿using IceCreamForm.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace IceCreamForm
{
    public partial class EditCustomer : IceCreamForm.ParentForm
    {
        private DataRepository dataRepository = new DataRepository();
        Customer currentCustomer;

        public EditCustomer(int customerId)
        {
            InitializeComponent();
            currentCustomer = dataRepository.GetCustomerByCustomerId(customerId);
            txtFName.Text = currentCustomer.FName;
            txtLName.Text = currentCustomer.LName;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            try
            {
                currentCustomer.FName = txtFName.Text;
                currentCustomer.LName = txtLName.Text;
                dataRepository.EditCustomer(currentCustomer, DelegateHelper.DisplayMessage);
                txtFName.Clear();
                txtLName.Clear();
                RedirectToListCustomers();
            }
            catch (Exception exception)
            {
                MessageBox.Show($"Failed to edit customer: {exception.Message}");
            }
            
        }
    }
}
