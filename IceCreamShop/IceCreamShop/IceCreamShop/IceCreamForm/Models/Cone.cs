﻿using System;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization;
using System.Text;

namespace IceCreamForm.Models
{
    public abstract class Cone
    {
        public int ConeId { get; set; }
        public int CustomerId { get; set; }
        public string ConeType { get; set; }
        public string Flavor { get; set; }
        public int Scoops { get; set; }

        private const decimal SCOOPCOST = 0.50M;
        private const decimal CONECOST = 0.75M;

        public abstract int GetChoiceId();
        public abstract string GetChoiceName();
        public decimal CalcCost()
        {
            return Scoops * SCOOPCOST + CONECOST;
        }

        public string ToFileString()
        {
            return $"{ConeId}|{CustomerId}|{GetChoiceId()}|{Flavor}|{Scoops}|{ConeType}".Replace("\n", "");
        }
        public override string ToString()
        {
            return $"{GetChoiceName()} Cone with Flavor:{Flavor}, ConeType: {ConeType}, scoops: {Scoops.ToString()}, Cost: {CalcCost().ToString("C2")}";
        }

    }
}
