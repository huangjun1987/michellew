﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IceCreamForm.Models
{
    public class Customer
    {
        private int customerID;
        private string fname;
        private string lname;

        public int CustomerID
        {
            get
            {
                return customerID > 0 ? customerID : 0;
            }
            set
            {
                if (value > 0)
                {
                    customerID = value;
                }
                else
                {
                    throw new CustomerException($"Entry \"{value.ToString()}\" is not valid.\nCustomer ID must be greater than 0");
                }
            }
        }
        public string LName
        {
            get { return lname; }
            set
            {
                if (value.Length > 0)
                {
                    lname = value.Trim();
                }
                else
                {
                    throw new CustomerException("Last name must not be empty");
                }
            }
        }
        public string FName
        {
            get { return fname; }
            set
            {
                if (value.Length > 0)
                {
                    fname = value.Trim();
                }
                else
                {
                    throw new CustomerException("First name must not be empty");
                }
            }
        }
        public string FullName
        {
            get { return $"{FName} {LName}"; }

        }
        public Customer() { }
        public Customer(int CustomerID, string LName, string FName)
        {
            this.CustomerID = CustomerID;
            this.LName = LName;
            this.FName = FName;
        }
        public Customer(string fileString)
        {
            string[] splitString = fileString.Split('|');
            try
            {
                CustomerID = Convert.ToInt32(splitString[0]);
                LName = splitString[1];
                FName = splitString[2];
            }
            catch (Exception e)
            {
                Console.WriteLine($"There was an error reading customer string '{fileString}'");
                Console.WriteLine(e.Message);
            }

        }



        public override string ToString()
        {
            return $"{customerID}|{LName}|{FName}";
        }

        [Serializable]
        public class CustomerException : Exception
        {
            public CustomerException(string message) : base(message)
            {
            }
        }
    }
}
