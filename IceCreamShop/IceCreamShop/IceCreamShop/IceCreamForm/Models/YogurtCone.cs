﻿namespace IceCreamForm.Models
{
    public class YogurtCone : Cone
    {
        private const int choiceId = 2;
        private const string choiceName = "Yogurt";
        public override int GetChoiceId()
        {
            return choiceId;
        }
        public override string GetChoiceName()
        {
            return choiceName;
        }

    }
}
