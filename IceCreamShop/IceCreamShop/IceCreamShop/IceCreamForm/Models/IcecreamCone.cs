﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IceCreamForm.Models
{
    public class IcecreamCone : Cone
    {
        private const int icecreamType = 1;
        private const string choiceName = "Icecream";
        public override int GetChoiceId()
        {
            return icecreamType;
        }
        public override string GetChoiceName()
        {
            return choiceName;
        }
    }
}
