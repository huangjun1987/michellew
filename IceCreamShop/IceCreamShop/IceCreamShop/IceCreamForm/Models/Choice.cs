﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IceCreamForm.Models
{
    public class Choice
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
