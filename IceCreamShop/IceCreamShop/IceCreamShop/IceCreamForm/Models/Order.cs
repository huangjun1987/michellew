﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace IceCreamForm.Models
{
    public class Order
    {

        public Customer Customer { get; set; }
        public Cone[] Cones { get; set; }


        public Order()
        {
            Customer = new Customer();
            Cones = new Cone[10];
        }

        public Order(Customer c)
        {
            Customer = c;
            Cones = new Cone[10];
        }


        public decimal CalcTotal()
        {
            decimal total = 0.00M;
            foreach (var cone in Cones.Where(c => c != null))
            {
                total += cone.CalcCost();
            }
            return total;
        }

        public void AddCone(Cone cone)
        {
            for (int i = 0; i < 10; i++)
            {
                if (Cones[i] == null)
                {
                    Cones[i] = cone;
                    return;
                }
            }
        }
    }
}
