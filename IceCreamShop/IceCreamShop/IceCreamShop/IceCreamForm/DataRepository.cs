﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IceCreamForm.Models;

namespace IceCreamForm
{
    public class DataRepository
    {
        public List<Customer> Customers { get; set; }
        public List<Order> Orders { get; set; }
        private StreamReader reader;
        private StreamWriter writer;
        private const string customerFileName = "Customer.txt";
        private const string orderFileName = "Order.txt";

        internal Customer GetCustomerByCustomerId(int customerId)
        {
            return Customers.Where(c => c.CustomerID == customerId).FirstOrDefault();
        }

        public List<Choice> Choices { get; set; }

        public DataRepository()
        {
            GetCustomers();
            GetIcecreamTypes();
            GetOrders();
        }

        private void GetOrders()
        {
            Orders = new List<Order>();
            reader = new StreamReader(orderFileName);
            string line;
            do
            {
                line = reader.ReadLine();
                if (!string.IsNullOrEmpty(line))
                {
                    var fields = line.Split('|');
                    var coneId = int.Parse(fields[0]);
                    var customerId = int.Parse(fields[1]);
                    var customer = Customers.Where(c => c.CustomerID == customerId).FirstOrDefault();
                    if (customer != null)
                    {
                        var order = Orders.Where(o => o.Customer.CustomerID == customerId).FirstOrDefault();
                        if (order == null)
                        {
                            order = new Order(customer);
                            Orders.Add(order);
                        }
                        var type = int.Parse(fields[2]);
                        var cone = ConeFactory.GetCone(type);
                        cone.ConeId = coneId;
                        cone.CustomerId = customerId;
                        cone.Flavor = fields[3];
                        cone.Scoops = int.Parse(fields[4]);
                        cone.ConeType = fields[5];
                        order.AddCone(cone);
                    }

                }
            } while (!string.IsNullOrEmpty(line));
            reader.Close();
        }

        internal void EditCone(Order currentOrder, Cone updatedCone, Action<string> DisplayMessage)
        {
            var order = GetOrderByCustomerId(currentOrder.Customer.CustomerID);
            for (int i = 0; i < order.Cones.Length; i++)
            {
                if (order.Cones[i]!=null && order.Cones[i].ConeId == updatedCone.ConeId)
                {
                    order.Cones[i] = updatedCone;
                }
            }
            SaveOrders();
            DisplayMessage($"Successfully updated order: {updatedCone.ToString()}");
        }

        internal void EditCustomer(Customer currentCustomer, Action<string> DisplayMessage)
        {
            var customer = GetCustomerByCustomerId(currentCustomer.CustomerID);
            if (customer != null)
            {
                customer.FName = currentCustomer.FName;
                customer.LName = currentCustomer.LName;
            }
            else
            {
                DisplayMessage($"Can't find a customer with the customerId: {currentCustomer.CustomerID}");
            }
            SaveCustomers();
            DisplayMessage($"Successfully updated customer: {customer.ToString()}");
        }

        private void GetIcecreamTypes()
        {
            Choices = new List<Choice>();
            Choices.Add(new Choice() { Id = 1, Name = "Ice cream" });
            Choices.Add(new Choice() { Id = 2, Name = "Yogurt" });
        }

        internal string GetTotal(int customerID)
        {
            var order = GetOrderByCustomerId(customerID);
            var total = order.CalcTotal();
            return total.ToString("c2");
        }

        public Order GetOrderByCustomerId(int customerId)
        {
            var order =  Orders.Where(o => o.Customer != null && o.Customer.CustomerID == customerId).FirstOrDefault();
            if (order == null)
            {
                var customer = Customers.Where(c => c.CustomerID == customerId).FirstOrDefault();
                order = new Order(customer);
                Orders.Add(order);
            }

            return order;
        }
        private void GetCustomers()
        {
            Customers = new List<Customer>();
            reader = new StreamReader(customerFileName);
            string line;
            do
            {
                line = reader.ReadLine();
                if (!string.IsNullOrEmpty(line))
                {
                    var customer = new Customer(line);
                    Customers.Add(customer);
                }
            } while (!string.IsNullOrEmpty(line));

            reader.Close();
        }

        internal bool CanAddCone(int customerID)
        {
            var order = GetOrderByCustomerId(customerID);
            return order.Cones[9] == null;
        }

        internal void AddCone(int customerId, int icecreamType, string flavor, int numOfScoops, string coneType,Action<string> DisplayMessage)
        {
            if (!CanAddCone(customerId))
            {
                DisplayMessage("The current order has already reached the maximum number of cones. ");
                return;
            }
            var coneId = GetNextConeID();
            var order = GetOrderByCustomerId(customerId);
            var cone = ConeFactory.GetCone(icecreamType);
            if (cone != null)
            {
                cone.ConeId = coneId;
                cone.CustomerId = customerId;
                cone.Flavor = flavor;
                cone.Scoops = numOfScoops;
                cone.ConeType = coneType;
                AddConeToOrder(cone, order);
                writer = new StreamWriter(orderFileName, true);
                writer.WriteLine(cone.ToFileString());
                writer.Close();
                DisplayMessage($"Successfully added cone: {cone.ToString()}");
            }
        }

        private int GetNextConeID()
        {
            var maxConeId = 0;

            foreach (var order in Orders)
            {
                if (order.Cones.Any(c => c != null))
                {
                    var maxConeIdForOrder = order.Cones.Where(c => c != null).Select(c => c.ConeId).Max();
                    if (maxConeIdForOrder > maxConeId)
                    {
                        maxConeId = maxConeIdForOrder;
                    }
                }             
            }

            return maxConeId + 1;
        }

        private void AddConeToOrder(Cone cone, Order order)
        {
            for (int i = 0; i < 10; i++)
            {
                if (order.Cones[i] == null)
                {
                    order.Cones[i] = cone;
                    return;
                }
            }
        }

        internal bool CanDeleteCone(int customerID)
        {
            var order = GetOrderByCustomerId(customerID);
            return order.Cones[0] != null;
        }

        internal void DeleteCone(int customerId, int coneId, Action<string> DisplayMessage)
        {
            if (!CanDeleteCone(customerId))
            {
                DisplayMessage("The current order is empty. ");
                return;
            }
            var order = GetOrderByCustomerId(customerId);
            for (int i = 0; i < 10; i++)
            {
                if (order.Cones[i].ConeId == coneId)
                {
                    var message = $"Successfully deleted cone: {order.Cones[i].ToString()}.";
                    for (int j = i; j < 9; j++)
                    {
                        order.Cones[j] = order.Cones[j+1];
                    }                  
                    SaveOrders();
                    DisplayMessage(message);
                    return;
                }
            }
        }

        public bool CustomerExists(string lastName, string firstName)
        {
            return Customers.Any(c=>c.LName.Equals(lastName, StringComparison.CurrentCultureIgnoreCase) && c.FName.Equals(firstName));
        }
        public void AddCustomer(string lastName, string firstName, Action<string> DisplayMessage)
        {
            if (CustomerExists(lastName, firstName))
            {
                DisplayMessage("A customer with the same last name and first name already exists!");
                return;
            }
            var newCustomerId = Customers.Select(c => c.CustomerID).Max() + 1;
            var customer = new Customer(newCustomerId, lastName, firstName);
            var output = customer.ToString();
            writer = new StreamWriter(customerFileName, true);
            writer.WriteLine(output);
            writer.Close();
            Customers.Add(customer);
            DisplayMessage($"Successfully added customer: {firstName} {lastName}");
        }

        public void DeleteCustomer(int customerId, Action<string> DisplayMessage)
        {
            var orderToDelete = GetOrderByCustomerId(customerId);
            Orders.Remove(orderToDelete);
            SaveOrders();
            var customer = GetCustomerByCustomerId(customerId);
            if (customer != null)
            {
                Customers.Remove(customer);
            }
            else
            {
                DisplayMessage($"Can't find a customer with the customerId: {customerId}");
            }
            SaveCustomers();
            DisplayMessage($"Successfully deleted customer: {customer.ToString()}");
        }

        private void SaveCustomers()
        {
            writer = new StreamWriter(customerFileName);
            foreach (var customer in Customers)
            {
                writer.WriteLine(customer.ToString());
            }
            writer.Close();
        }

        private void SaveOrders()
        {
            writer = new StreamWriter(orderFileName);

            foreach (var order in Orders)
            {
                for (int i = 0; i < 10; i++)
                {
                    if (order.Cones[i] != null)
                    {
                        writer.WriteLine(order.Cones[i].ToFileString());
                    }
                }
            }
            writer.Close();
        }
    }
}
