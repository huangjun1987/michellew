﻿using IceCreamForm.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IceCreamForm
{
    public partial class DisplayOrder : Form
    {
        private DataRepository dataRepository = new DataRepository();
        Order currentOrder;
        public DisplayOrder(int customerId)
        {
            InitializeComponent();
            currentOrder = dataRepository.GetOrderByCustomerId(customerId);
            LoadCurrentOrder();
        }

        private void LoadCurrentOrder()
        {
            labelCustomerName.Text = $"{currentOrder.Customer.FName} {currentOrder.Customer.LName}";
            labelTotal.Text = dataRepository.GetTotal(currentOrder.Customer.CustomerID);
            listViewCones.Items.Clear();
            listViewCones.View = System.Windows.Forms.View.Details;
            listViewCones.Columns.Add("ConeId", 20);
            listViewCones.Columns.Add("Choice", 100);
            listViewCones.Columns.Add("Flavor", 100);
            listViewCones.Columns.Add("ConeType", 100);
            listViewCones.Columns.Add("Scoops", 100);
            listViewCones.Columns.Add("Cost", 100);
            for (int i = 0; i < currentOrder.Cones.Length; i++)
            {
                var cone = currentOrder.Cones[i];
                if (cone != null)
                {
                    listViewCones.Items.Add(cone.ConeId.ToString());
                    listViewCones.Items[i].SubItems.Add(cone.GetChoiceName());
                    listViewCones.Items[i].SubItems.Add(cone.Flavor);
                    listViewCones.Items[i].SubItems.Add(cone.ConeType);
                    listViewCones.Items[i].SubItems.Add(cone.Scoops.ToString());
                    listViewCones.Items[i].SubItems.Add(cone.CalcCost().ToString());
                }
            }
        }

        private void buttonAddCone_Click(object sender, EventArgs e)
        {
            this.Hide();
            var form = new AddCone(currentOrder);
            form.Show();
        }

        private void buttonEditCone_Click(object sender, EventArgs e)
        {
            if (listViewCones.SelectedItems.Count < 1)
            {
                MessageBox.Show("Please select a cone!");
                return;
            }
            var selectedRow = listViewCones.SelectedItems[0];
            this.Hide();
            var form = new EditCone(currentOrder, int.Parse(selectedRow.Text));
            form.Show();
       
        }

        private void buttonDeleteCone_Click(object sender, EventArgs e)
        {
            try
            {
                if (listViewCones.SelectedItems.Count < 1)
                {
                    MessageBox.Show("Please select a cone!");
                    return;
                }
                var selectedRow = listViewCones.SelectedItems[0];
                dataRepository.DeleteCone(currentOrder.Customer.CustomerID, int.Parse(selectedRow.Text), DelegateHelper.DisplayMessage);
                LoadCurrentOrder();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Failed to delete cone: {ex.Message}");
            }
        }

        private void buttonViewCustomers_Click(object sender, EventArgs e)
        {
            this.Hide();
            var form = new ListCustomers();
            form.Show();
        }
    }
}
