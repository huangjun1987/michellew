﻿using IceCreamForm.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace IceCreamForm
{
    public partial class AddCone : IceCreamForm.ParentFormOrder
    {
        public AddCone(Order order):base(order)
        {
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedIcecreamType = (Choice)comboBoxChoice.SelectedItem ?? dataRepository.Choices[0];
                dataRepository.AddCone(currentOrder.Customer.CustomerID, selectedIcecreamType.Id, comboBoxFlavor.SelectedItem.ToString(), (int)numScoops.Value, comboBoxConeType.SelectedItem.ToString(), DelegateHelper.DisplayMessage);
                this.Hide();
                var form = new DisplayOrder(currentOrder.Customer.CustomerID);
                form.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Failed to add cone: {ex.Message}");
            }
        }
    }
}
