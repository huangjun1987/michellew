﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IceCreamForm.Models;

namespace IceCreamForm
{
    public static class ConeFactory
    {
        public static Cone GetCone(int type)
        {
            switch (type)
            {
                case 1:
                    return new IcecreamCone();
                case 2:
                    return new YogurtCone();
                default:
                    return null;
            }
        }
    }
}
