﻿using IceCreamForm.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IceCreamForm
{
    public partial class ListCustomers : Form
    {
        Customer currentCustomer;
        private DataRepository dataRepository = new DataRepository();
        public ListCustomers()
        {
            InitializeComponent();
            LoadAllCustomers();
        }

        private void LoadAllCustomers()
        {
            listViewCustomers.Items.Clear();
            listViewCustomers.View = System.Windows.Forms.View.Details;
            listViewCustomers.Columns.Add("CustomerId", 100);
            listViewCustomers.Columns.Add("FirstName", 100);
            listViewCustomers.Columns.Add("LastName", 100);

            var customers = dataRepository.Customers.ToArray();

            if (customers.Length > 0)
            {
                for (int i = 0; i < customers.Length; i++)
                {
                    var customer = customers[i];
                    listViewCustomers.Items.Add(customer.CustomerID.ToString());
                    listViewCustomers.Items[i].SubItems.Add(customer.FName);
                    listViewCustomers.Items[i].SubItems.Add(customer.LName);
                }
            }
        }

        private void buttonAddCustomer_Click(object sender, EventArgs e)
        {
            this.Hide();
            var form = new AddCustomer();
            form.Show();
        }

        private void buttonEditCustomer_Click(object sender, EventArgs e)
        {
            if (listViewCustomers.SelectedItems.Count < 1)
            {
                MessageBox.Show("Please select a customer!");
                return;
            }
            var selectedRow = listViewCustomers.SelectedItems[0];
            this.Hide();
            var form = new EditCustomer(int.Parse(selectedRow.Text));
            form.Show();
        }

        private void buttonDeleteCustomer_Click(object sender, EventArgs e)
        {
            try
            {
                if (listViewCustomers.SelectedItems.Count < 1)
                {
                    MessageBox.Show("Please select a customer!");
                    return;
                }
                var selectedRow = listViewCustomers.SelectedItems[0];
                dataRepository.DeleteCustomer(int.Parse(selectedRow.Text), DelegateHelper.DisplayMessage);
                LoadAllCustomers();                   
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Failed to delete customer: {ex.Message}");
            }
            
        }

        private void buttonViewOrder_Click(object sender, EventArgs e)
        {
            if (listViewCustomers.SelectedItems.Count < 1)
            {
                MessageBox.Show("Please select a customer!");
                return;
            }
            var selectedRow = listViewCustomers.SelectedItems[0];
            this.Hide();
            var form = new DisplayOrder(int.Parse(selectedRow.Text));
            form.Show();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
