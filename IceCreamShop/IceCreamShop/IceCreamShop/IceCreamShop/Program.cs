﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using ICData;

namespace IceCreamShop
{
    class Program
    {
        static List<Customer> allCustomers;
        static List<Order> allOrders;

        static void Main(string[] args)
        {
            Cone superCone = new Cone("IC", "Peanut Butter", 2) + new Cone("YO", "Jelly", 3);
            Console.WriteLine(superCone.ToString());

            TestCustomers();
            TestOrders();

            Console.WriteLine("Press the any key. . .");
            Console.ReadKey();
        }

        public static bool KeepGoing(string message)
        {
            string input = "";
            while (!input.ToUpper().StartsWith("Y") && !input.ToUpper().StartsWith("N"))
            {
                Console.Write("\n" + message);
                input = Console.ReadLine();

                if (input.ToUpper().StartsWith("Y"))
                {
                    return true;
                }
                else if (input.ToUpper().StartsWith("N"))
                {
                    return false;
                }
            }
            return false;
        }

        private static void TestCustomers()
        {
            Customer[] savedCustomers = DIO.ReadAll(Path.Combine(Environment.CurrentDirectory, ConfigurationManager.AppSettings.Get("CustomerFile"))).Select(s => new Customer(s)).ToArray();
            allCustomers = savedCustomers.Select(c => c).ToList();

            foreach (Customer c in savedCustomers)
            {
                Console.WriteLine(c.ToString());
            }

            Customer c1 = Customer.ReadCustomer(1);
            for (int x = 0; x < 5; x++)
            {
                Console.WriteLine($"His name is {c1.FName} {c1.LName}");
            }

            while (KeepGoing("Enter new customer? y/n "))
            {
                try
                {
                    string input;
                    Customer newCust = new Customer();

                    int nextID;
                    try
                    {
                        nextID = savedCustomers.Max(c => c.CustomerID) + 1;
                    }
                    catch (Exception)
                    {
                        nextID = 1;
                    }
                    newCust.CustomerID = nextID;

                    Console.Write("Enter First Name ");
                    input = Console.ReadLine();
                    newCust.FName = input;

                    Console.Write("Enter Last Name ");
                    input = Console.ReadLine();
                    newCust.LName = input;

                    if (newCust.SaveCustomer())
                    {
                        Console.WriteLine("Customer saved successfully!");
                        allCustomers.Add(newCust);
                    }
                    else
                    {
                        Console.WriteLine("There was a problem saving the customer.");
                    }
                }
                catch (Customer.CustomerException ce)
                {
                    Console.WriteLine(ce.Message);
                }
                catch (Exception)
                {
                    Console.WriteLine("Invalid entry");
                }
            }

            Customer deleteTestCustomer = new Customer(allCustomers.Max(c => c.CustomerID) + 1, "Testerson", "Test");
            if (deleteTestCustomer.SaveCustomer())
            {
                Console.WriteLine("Customer to delete saved successfully!");
            }
            if (deleteTestCustomer.DeleteCustomer(allCustomers.Count))
            {
                Console.WriteLine("Customer deleted successfully!");
            }
        }

        private static void TestOrders()
        {
            allOrders = Order.ReadAllOrders();
            Order[] savedOrders = allOrders.Select(o => o).ToArray();


            while (KeepGoing("Select or enter an order? y/n "))
            {
                try
                {
                    string input;
                    Console.Write("Enter customer ID ");
                    input = Console.ReadLine();
                    Customer customer = allCustomers.Where(c => c.CustomerID == Convert.ToInt32(input)).FirstOrDefault();

                    if (savedOrders.Any(o => o.Customer.CustomerID == customer.CustomerID))
                    {   //Append order
                        Order currentOrder = Order.ReadOrder(customer.CustomerID);
                        int coneCount = currentOrder.Cones.Where(c => c != null).Count();
                        if (coneCount < 10)
                        {
                            InputCone(currentOrder, ref coneCount);
                        }
                    }
                    else if (allCustomers.Any(c => c.CustomerID == customer.CustomerID))
                    {   //Create new order
                        Order o = new Order(customer);
                        int coneCount = 0;
                        InputCone(o, ref coneCount);
                    }
                    else
                    {
                        Console.WriteLine("That customer doesn't exist");
                    }
                }
                catch (Cone.ConeException ce)
                {
                    Console.WriteLine(ce.Message);
                }
                catch (Exception)
                {
                    Console.WriteLine("Invalid entry");
                }
            }
        }

        private static void InputCone(Order currentOrder, ref int coneCount)
        {
            string input;
            int i;
            if (currentOrder.Cones.Any(c => c != null))
            {
                Console.WriteLine("Customer has an existing order");
                currentOrder.DisplayOrder();
            }
            else
            {
                Console.WriteLine("Creating new order");
            }

            while (KeepGoing("Add cone? y/n ") && coneCount < 10)
            {
                Cone newCone = currentOrder.Cones.Where(c => c == null).First();

                Console.Write("Ice cream or frozen yogurt?\nIC for ice cream\nYO for yogurt");
                input = Console.ReadLine();
                if (input.ToUpper().StartsWith("IC") || input.ToUpper().StartsWith("YO"))
                {
                    newCone = new Cone
                    {
                        ConeType = input.Substring(0, 2).ToUpper()
                    };
                }
                else
                {
                    Console.WriteLine("Invalid entry");
                    input = "Y";
                    continue;
                }

                Console.WriteLine("You chose " + newCone.CheckType());

                input = "";

                while (input.Length <= 0)
                {

                    Console.Write("What flavor? ");
                    input = Console.ReadLine();

                    if (input.Length > 0)
                    {
                        newCone.Flavor = input;
                        input = "Y";
                    }
                    else
                    {
                        Console.WriteLine("Flavor cannot be blank");
                        input = "";
                    }
                }

                while (!int.TryParse(input, out i) || i <= 0)
                {
                    Console.Write("How many scoops? ");
                    input = Console.ReadLine();
                    int.TryParse(input, out i);
                    if (i > 0)
                    {
                        newCone.Scoops = i;
                    }
                    else
                    {
                        Console.WriteLine("Invalid number of scoops, try again");
                        input = "Y";
                    }
                }
                if (newCone.SaveCone(currentOrder.Customer.CustomerID))
                {
                    coneCount++;
                    Console.WriteLine("Cone saved to order!");
                }
                else
                {
                    Console.WriteLine("Cone save failed.");
                }
            }
        }
    }


}
