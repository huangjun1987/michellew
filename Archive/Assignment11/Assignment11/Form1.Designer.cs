﻿namespace Assignment11
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TabControl = new System.Windows.Forms.TabControl();
            this.Entry = new System.Windows.Forms.TabPage();
            this.buttonExit = new System.Windows.Forms.Button();
            this.buttonEnterFriend = new System.Windows.Forms.Button();
            this.maskedTextBoxPhoneNumber = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxBirthday = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxBirthMonth = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxLastName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxFirstName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Read = new System.Windows.Forms.TabPage();
            this.listBoxRead = new System.Windows.Forms.ListBox();
            this.buttonExitRead = new System.Windows.Forms.Button();
            this.buttonRead = new System.Windows.Forms.Button();
            this.Reminder = new System.Windows.Forms.TabPage();
            this.listBoxReminder = new System.Windows.Forms.ListBox();
            this.buttonExitReminder = new System.Windows.Forms.Button();
            this.buttonReminder = new System.Windows.Forms.Button();
            this.textBoxBirthMonthReminder = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TabControl.SuspendLayout();
            this.Entry.SuspendLayout();
            this.Read.SuspendLayout();
            this.Reminder.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabControl
            // 
            this.TabControl.Controls.Add(this.Entry);
            this.TabControl.Controls.Add(this.Read);
            this.TabControl.Controls.Add(this.Reminder);
            this.TabControl.Location = new System.Drawing.Point(0, 4);
            this.TabControl.Margin = new System.Windows.Forms.Padding(6);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedIndex = 0;
            this.TabControl.Size = new System.Drawing.Size(1604, 880);
            this.TabControl.TabIndex = 0;
            this.TabControl.SelectedIndexChanged += new System.EventHandler(this.TabControl_SelectedIndexChanged);
            // 
            // Entry
            // 
            this.Entry.Controls.Add(this.buttonExit);
            this.Entry.Controls.Add(this.buttonEnterFriend);
            this.Entry.Controls.Add(this.maskedTextBoxPhoneNumber);
            this.Entry.Controls.Add(this.label5);
            this.Entry.Controls.Add(this.textBoxBirthday);
            this.Entry.Controls.Add(this.label4);
            this.Entry.Controls.Add(this.textBoxBirthMonth);
            this.Entry.Controls.Add(this.label3);
            this.Entry.Controls.Add(this.textBoxLastName);
            this.Entry.Controls.Add(this.label2);
            this.Entry.Controls.Add(this.textBoxFirstName);
            this.Entry.Controls.Add(this.label1);
            this.Entry.Location = new System.Drawing.Point(10, 48);
            this.Entry.Margin = new System.Windows.Forms.Padding(6);
            this.Entry.Name = "Entry";
            this.Entry.Padding = new System.Windows.Forms.Padding(6);
            this.Entry.Size = new System.Drawing.Size(1584, 822);
            this.Entry.TabIndex = 0;
            this.Entry.Text = "Entry";
            this.Entry.UseVisualStyleBackColor = true;
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(428, 568);
            this.buttonExit.Margin = new System.Windows.Forms.Padding(6);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(150, 45);
            this.buttonExit.TabIndex = 11;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // buttonEnterFriend
            // 
            this.buttonEnterFriend.Location = new System.Drawing.Point(60, 570);
            this.buttonEnterFriend.Margin = new System.Windows.Forms.Padding(6);
            this.buttonEnterFriend.Name = "buttonEnterFriend";
            this.buttonEnterFriend.Size = new System.Drawing.Size(290, 45);
            this.buttonEnterFriend.TabIndex = 10;
            this.buttonEnterFriend.Text = "Enter Friend";
            this.buttonEnterFriend.UseVisualStyleBackColor = true;
            this.buttonEnterFriend.Click += new System.EventHandler(this.buttonEnterFriend_Click);
            // 
            // maskedTextBoxPhoneNumber
            // 
            this.maskedTextBoxPhoneNumber.Location = new System.Drawing.Point(370, 463);
            this.maskedTextBoxPhoneNumber.Margin = new System.Windows.Forms.Padding(6);
            this.maskedTextBoxPhoneNumber.Mask = "(999) 000-0000";
            this.maskedTextBoxPhoneNumber.Name = "maskedTextBoxPhoneNumber";
            this.maskedTextBoxPhoneNumber.Size = new System.Drawing.Size(196, 38);
            this.maskedTextBoxPhoneNumber.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(54, 463);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(205, 32);
            this.label5.TabIndex = 8;
            this.label5.Text = "Phone Number";
            // 
            // textBoxBirthday
            // 
            this.textBoxBirthday.Location = new System.Drawing.Point(370, 355);
            this.textBoxBirthday.Margin = new System.Windows.Forms.Padding(6);
            this.textBoxBirthday.Name = "textBoxBirthday";
            this.textBoxBirthday.Size = new System.Drawing.Size(196, 38);
            this.textBoxBirthday.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(54, 355);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 32);
            this.label4.TabIndex = 6;
            this.label4.Text = "Birthday";
            // 
            // textBoxBirthMonth
            // 
            this.textBoxBirthMonth.Location = new System.Drawing.Point(370, 250);
            this.textBoxBirthMonth.Margin = new System.Windows.Forms.Padding(6);
            this.textBoxBirthMonth.Name = "textBoxBirthMonth";
            this.textBoxBirthMonth.Size = new System.Drawing.Size(196, 38);
            this.textBoxBirthMonth.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(54, 250);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(160, 32);
            this.label3.TabIndex = 4;
            this.label3.Text = "Birth Month";
            // 
            // textBoxLastName
            // 
            this.textBoxLastName.Location = new System.Drawing.Point(370, 145);
            this.textBoxLastName.Margin = new System.Windows.Forms.Padding(6);
            this.textBoxLastName.Name = "textBoxLastName";
            this.textBoxLastName.Size = new System.Drawing.Size(196, 38);
            this.textBoxLastName.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 145);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 32);
            this.label2.TabIndex = 2;
            this.label2.Text = "Last Name";
            // 
            // textBoxFirstName
            // 
            this.textBoxFirstName.Location = new System.Drawing.Point(370, 50);
            this.textBoxFirstName.Margin = new System.Windows.Forms.Padding(6);
            this.textBoxFirstName.Name = "textBoxFirstName";
            this.textBoxFirstName.Size = new System.Drawing.Size(196, 38);
            this.textBoxFirstName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(54, 50);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "First Name";
            // 
            // Read
            // 
            this.Read.Controls.Add(this.listBoxRead);
            this.Read.Controls.Add(this.buttonExitRead);
            this.Read.Controls.Add(this.buttonRead);
            this.Read.Location = new System.Drawing.Point(10, 48);
            this.Read.Margin = new System.Windows.Forms.Padding(6);
            this.Read.Name = "Read";
            this.Read.Padding = new System.Windows.Forms.Padding(6);
            this.Read.Size = new System.Drawing.Size(1584, 822);
            this.Read.TabIndex = 1;
            this.Read.Text = "Read";
            this.Read.UseVisualStyleBackColor = true;
            // 
            // listBoxRead
            // 
            this.listBoxRead.FormattingEnabled = true;
            this.listBoxRead.ItemHeight = 31;
            this.listBoxRead.Location = new System.Drawing.Point(52, 159);
            this.listBoxRead.Margin = new System.Windows.Forms.Padding(6);
            this.listBoxRead.Name = "listBoxRead";
            this.listBoxRead.Size = new System.Drawing.Size(697, 159);
            this.listBoxRead.TabIndex = 2;
            // 
            // buttonExitRead
            // 
            this.buttonExitRead.Location = new System.Drawing.Point(294, 50);
            this.buttonExitRead.Margin = new System.Windows.Forms.Padding(6);
            this.buttonExitRead.Name = "buttonExitRead";
            this.buttonExitRead.Size = new System.Drawing.Size(150, 45);
            this.buttonExitRead.TabIndex = 1;
            this.buttonExitRead.Text = "Exit";
            this.buttonExitRead.UseVisualStyleBackColor = true;
            this.buttonExitRead.Click += new System.EventHandler(this.buttonExitRead_Click);
            // 
            // buttonRead
            // 
            this.buttonRead.Location = new System.Drawing.Point(52, 52);
            this.buttonRead.Margin = new System.Windows.Forms.Padding(6);
            this.buttonRead.Name = "buttonRead";
            this.buttonRead.Size = new System.Drawing.Size(150, 45);
            this.buttonRead.TabIndex = 0;
            this.buttonRead.Text = "Read";
            this.buttonRead.UseVisualStyleBackColor = true;
            this.buttonRead.Click += new System.EventHandler(this.buttonRead_Click);
            // 
            // Reminder
            // 
            this.Reminder.Controls.Add(this.listBoxReminder);
            this.Reminder.Controls.Add(this.buttonExitReminder);
            this.Reminder.Controls.Add(this.buttonReminder);
            this.Reminder.Controls.Add(this.textBoxBirthMonthReminder);
            this.Reminder.Controls.Add(this.label6);
            this.Reminder.Location = new System.Drawing.Point(10, 48);
            this.Reminder.Margin = new System.Windows.Forms.Padding(6);
            this.Reminder.Name = "Reminder";
            this.Reminder.Padding = new System.Windows.Forms.Padding(6);
            this.Reminder.Size = new System.Drawing.Size(1584, 822);
            this.Reminder.TabIndex = 2;
            this.Reminder.Text = "Reminder";
            this.Reminder.UseVisualStyleBackColor = true;
            // 
            // listBoxReminder
            // 
            this.listBoxReminder.FormattingEnabled = true;
            this.listBoxReminder.ItemHeight = 31;
            this.listBoxReminder.Location = new System.Drawing.Point(60, -57);
            this.listBoxReminder.Margin = new System.Windows.Forms.Padding(6);
            this.listBoxReminder.Name = "listBoxReminder";
            this.listBoxReminder.Size = new System.Drawing.Size(735, 159);
            this.listBoxReminder.TabIndex = 10;
            // 
            // buttonExitReminder
            // 
            this.buttonExitReminder.Location = new System.Drawing.Point(286, 128);
            this.buttonExitReminder.Margin = new System.Windows.Forms.Padding(6);
            this.buttonExitReminder.Name = "buttonExitReminder";
            this.buttonExitReminder.Size = new System.Drawing.Size(150, 45);
            this.buttonExitReminder.TabIndex = 9;
            this.buttonExitReminder.Text = "Exit";
            this.buttonExitReminder.UseVisualStyleBackColor = true;
            this.buttonExitReminder.Click += new System.EventHandler(this.buttonExitReminder_Click);
            // 
            // buttonReminder
            // 
            this.buttonReminder.Location = new System.Drawing.Point(44, 130);
            this.buttonReminder.Margin = new System.Windows.Forms.Padding(6);
            this.buttonReminder.Name = "buttonReminder";
            this.buttonReminder.Size = new System.Drawing.Size(188, 45);
            this.buttonReminder.TabIndex = 8;
            this.buttonReminder.Text = "Reminder";
            this.buttonReminder.UseVisualStyleBackColor = true;
            this.buttonReminder.Click += new System.EventHandler(this.buttonReminder_Click);
            // 
            // textBoxBirthMonthReminder
            // 
            this.textBoxBirthMonthReminder.Location = new System.Drawing.Point(234, 43);
            this.textBoxBirthMonthReminder.Margin = new System.Windows.Forms.Padding(6);
            this.textBoxBirthMonthReminder.Name = "textBoxBirthMonthReminder";
            this.textBoxBirthMonthReminder.Size = new System.Drawing.Size(196, 38);
            this.textBoxBirthMonthReminder.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(38, 43);
            this.label6.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(160, 32);
            this.label6.TabIndex = 6;
            this.label6.Text = "Birth Month";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1600, 872);
            this.Controls.Add(this.TabControl);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "Form1";
            this.Text = "Friends";
            this.TabControl.ResumeLayout(false);
            this.Entry.ResumeLayout(false);
            this.Entry.PerformLayout();
            this.Read.ResumeLayout(false);
            this.Reminder.ResumeLayout(false);
            this.Reminder.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl TabControl;
        private System.Windows.Forms.TabPage Entry;
        private System.Windows.Forms.TabPage Read;
        private System.Windows.Forms.TabPage Reminder;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxPhoneNumber;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxBirthday;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxBirthMonth;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxLastName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxFirstName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonEnterFriend;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Button buttonRead;
        private System.Windows.Forms.Button buttonExitRead;
        private System.Windows.Forms.ListBox listBoxRead;
        private System.Windows.Forms.TextBox textBoxBirthMonthReminder;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonReminder;
        private System.Windows.Forms.ListBox listBoxReminder;
        private System.Windows.Forms.Button buttonExitReminder;
    }
}

