﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment11
{
   public class Friend
   {
       private string lastName;
       private string phoneNumber;
       private int month;
       private int day;
       private string firstName;

       public int Month
       {
           get => month;
           set
           {
               if ((value > 0) && (value < 13))
               {
                   month = value;
               }
           }
       }
       public int Day
       {
           get => day;
           set
           {
               if ((value > 0) && (value < 31))
               {
                   day = value;
               }
           }
       }
       public string LastName
       {
           get => lastName;
           set
           {
               lastName = value;
           }
       }
       public string FirstName
       {
           get => firstName;
           set
           {
               firstName = value;
           }
       }
       public string PhoneNumber
       {
           get => phoneNumber;
           set
           {
               phoneNumber = value;
           }
       }

       public override string ToString()
       {
           return $"{FirstName},{LastName},{PhoneNumber},{Month},{Day}";
       }
   }
}
