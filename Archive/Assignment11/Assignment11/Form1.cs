﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Assignment11
{
    public partial class Form1 : Form
    {
        private Friend friend = new Friend();
        private StreamWriter streamWriter;
        private StreamReader streamReader;
        public Form1()
        {
            InitializeComponent();        
        }

        private void buttonEnterFriend_Click(object sender, EventArgs e)
        {
 
            streamWriter = new StreamWriter("Friends.txt", true);
            friend.FirstName = textBoxFirstName.Text;
            friend.LastName = textBoxLastName.Text;
            friend.Month = int.Parse(textBoxBirthMonth.Text);
            friend.Day = int.Parse(textBoxBirthday.Text);
            friend.PhoneNumber = maskedTextBoxPhoneNumber.Text;
            streamWriter.WriteLine(friend.ToString());
            MessageBox.Show(friend.ToString());
            streamWriter.Close();
            textBoxFirstName.Clear();
            textBoxLastName.Clear();
            textBoxBirthMonth.Clear();
            textBoxBirthday.Clear();
            maskedTextBoxPhoneNumber.Clear();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonRead_Click(object sender, EventArgs e)
        {
            listBoxRead.Items.Clear();
            streamReader = new StreamReader("Friends.txt");
            string line;
            do
            {
                line = streamReader.ReadLine();

                if (!string.IsNullOrEmpty(line))
                {
                    var fields = line.Split(',');  
                    var output = string.Join(" ", fields);                 
                    listBoxRead.Items.Add(output);
                }
            }
            while (!string.IsNullOrEmpty(line));
            streamReader.Close();
        }

        private void buttonExitRead_Click(object sender, EventArgs e)
        {
            buttonExit_Click(sender, e);
        }

        private void buttonExitReminder_Click(object sender, EventArgs e)
        {
            buttonExit_Click(sender, e);
        }

        private void buttonReminder_Click(object sender, EventArgs e)
        {
            listBoxReminder.Items.Clear();
            var selectedMonth = int.Parse(textBoxBirthMonthReminder.Text);
            streamReader = new StreamReader("Friends.txt");
            string line;
            do
            {
                line = streamReader.ReadLine();

                if (!string.IsNullOrEmpty(line))
                {
                    var fields = line.Split(',');
                    friend.FirstName = fields[0];
                    friend.LastName = fields[1];
                    friend.PhoneNumber = fields[2];
                    friend.Month = int.Parse(fields[3]);
                    friend.Day = int.Parse(fields[4]);
                    if (selectedMonth == friend.Month)
                    {
                       var output = $"{friend.FirstName} {friend.LastName} {friend.PhoneNumber} {friend.Month}/{friend.Day}";
                        listBoxReminder.Items.Add(output);
                    }
                }
            }
            while (!string.IsNullOrEmpty(line));

            textBoxBirthMonthReminder.Text = "";
            streamReader.Close();
        }

        private void TabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBoxReminder.Items.Clear();
            listBoxRead.Items.Clear();
        }
    }
}
