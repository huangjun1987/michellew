﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment8a
{
    internal class Assignment8 : System.Windows.Forms.Form
    {
        private System.Windows.Forms.Button btnClickMe;
        private System.Windows.Forms.Button btnExit;

        public Assignment8()
        {
            btnClickMe = new System.Windows.Forms.Button();
            btnExit = new System.Windows.Forms.Button();
            Text = "Assignment 8";
            ClientSize = new System.Drawing.Size(300, 150);
            btnClickMe.Text = "Press Me";
            btnExit.Text = "Exit";
            Controls.Add(btnClickMe);
            Controls.Add(btnExit);
            btnClickMe.Location = new System.Drawing.Point(15, 20);
            btnExit.Location = new System.Drawing.Point(15, 60);
            btnClickMe.Click += new System.EventHandler(btnClickMe_Click);
            btnExit.Click += new System.EventHandler(btnExit_Click);

        }

        private void btnClickMe_Click(object sender, EventArgs e)
        {
            MessageBox.Show("The Button Has Been Clicked", "Click Me");
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Assignment8());
        }
    }
}
