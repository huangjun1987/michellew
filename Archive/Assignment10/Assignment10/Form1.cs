﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment10
{
    public partial class Form1 : Form
    {
        private int[] randomNumbers = new int[100];
        private int currentIndex = 0;
        private int correctGuessCount = 0;
        private int incorrectGuessCount = 0;

        public Form1()
        {
            InitializeComponent();  
            GenerateRandonNumbers();
        }

        private void GenerateRandonNumbers()
        {
            var random = new System.Random();
            for (int i = 0; i < 100; i++)
            {
                randomNumbers[i] = random.Next(0, 100);
            }
        }

        private void label1_MouseHover(object sender, EventArgs e)
        {
            if (randomNumbers[currentIndex]>3 && randomNumbers[currentIndex] < 96)
            {
                labelHintResult.Text = $"It's not {randomNumbers[currentIndex] + 3}";
            }
            else
            {
                labelHintResult.Text = $"It's not {randomNumbers[currentIndex] - 1}";
            }
        }

        private void label1_MouseLeave(object sender, EventArgs e)
        {
            labelHintResult.Text = "";
        }

        private void buttonGuess_Click(object sender, EventArgs e)
        {
            buttonGuess.Enabled = false;
            buttonNextGuess.Enabled = true;
            int guessedNumber = int.Parse(textBoxGuessNumber.Text);
            if (guessedNumber == randomNumbers[currentIndex])
            {
                labelResult.Text = "You Win";
                correctGuessCount++;
                labelStats.Text = $"the number of correct guesses made: {correctGuessCount}";
            }
            else
            {
                labelResult.Text = $"Sorry - You Lose; the number is:  {randomNumbers[currentIndex]}";
                incorrectGuessCount++;
                labelStats.Text = $"the number of incorrect guesses made: {incorrectGuessCount}";
            }
            labelShowHint.Enabled = false;
            currentIndex++;
            if (currentIndex>99)
            {
                currentIndex = 0;
                GenerateRandonNumbers();
            }
        }

        private void buttonNextGuess_Click(object sender, EventArgs e)
        {
            textBoxGuessNumber.Text = "";
            labelResult.Text = "";
            buttonNextGuess.Enabled = false;
            buttonGuess.Enabled = true;
            labelStats.Text = "";
            labelShowHint.Enabled = true;
            
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
