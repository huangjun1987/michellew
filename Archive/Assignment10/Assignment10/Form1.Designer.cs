﻿namespace Assignment10
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxGuessNumber = new System.Windows.Forms.TextBox();
            this.buttonGuess = new System.Windows.Forms.Button();
            this.buttonNextGuess = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.labelShowHint = new System.Windows.Forms.Label();
            this.labelHintResult = new System.Windows.Forms.Label();
            this.labelResult = new System.Windows.Forms.Label();
            this.labelStats = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxGuessNumber
            // 
            this.textBoxGuessNumber.Location = new System.Drawing.Point(76, 45);
            this.textBoxGuessNumber.Name = "textBoxGuessNumber";
            this.textBoxGuessNumber.Size = new System.Drawing.Size(208, 38);
            this.textBoxGuessNumber.TabIndex = 0;
            // 
            // buttonGuess
            // 
            this.buttonGuess.Location = new System.Drawing.Point(76, 154);
            this.buttonGuess.Name = "buttonGuess";
            this.buttonGuess.Size = new System.Drawing.Size(144, 70);
            this.buttonGuess.TabIndex = 1;
            this.buttonGuess.Text = "Guess";
            this.buttonGuess.UseVisualStyleBackColor = true;
            this.buttonGuess.Click += new System.EventHandler(this.buttonGuess_Click);
            // 
            // buttonNextGuess
            // 
            this.buttonNextGuess.Enabled = false;
            this.buttonNextGuess.Location = new System.Drawing.Point(293, 154);
            this.buttonNextGuess.Name = "buttonNextGuess";
            this.buttonNextGuess.Size = new System.Drawing.Size(221, 70);
            this.buttonNextGuess.TabIndex = 2;
            this.buttonNextGuess.Text = "Next Guess";
            this.buttonNextGuess.UseVisualStyleBackColor = true;
            this.buttonNextGuess.Click += new System.EventHandler(this.buttonNextGuess_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(604, 154);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(144, 70);
            this.buttonExit.TabIndex = 3;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // labelShowHint
            // 
            this.labelShowHint.AutoSize = true;
            this.labelShowHint.Location = new System.Drawing.Point(418, 45);
            this.labelShowHint.Name = "labelShowHint";
            this.labelShowHint.Size = new System.Drawing.Size(422, 32);
            this.labelShowHint.TabIndex = 4;
            this.labelShowHint.Text = "Place your mouse here for a hint";
            this.labelShowHint.MouseLeave += new System.EventHandler(this.label1_MouseLeave);
            this.labelShowHint.MouseHover += new System.EventHandler(this.label1_MouseHover);
            // 
            // labelHintResult
            // 
            this.labelHintResult.AutoSize = true;
            this.labelHintResult.Location = new System.Drawing.Point(424, 94);
            this.labelHintResult.Name = "labelHintResult";
            this.labelHintResult.Size = new System.Drawing.Size(0, 32);
            this.labelHintResult.TabIndex = 5;
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Location = new System.Drawing.Point(76, 273);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(0, 32);
            this.labelResult.TabIndex = 7;
            // 
            // labelStats
            // 
            this.labelStats.AutoSize = true;
            this.labelStats.Location = new System.Drawing.Point(76, 357);
            this.labelStats.Name = "labelStats";
            this.labelStats.Size = new System.Drawing.Size(0, 32);
            this.labelStats.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1459, 669);
            this.Controls.Add(this.labelStats);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.labelHintResult);
            this.Controls.Add(this.labelShowHint);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.buttonNextGuess);
            this.Controls.Add(this.buttonGuess);
            this.Controls.Add(this.textBoxGuessNumber);
            this.Name = "Form1";
            this.Text = "GuessANumber";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxGuessNumber;
        private System.Windows.Forms.Button buttonGuess;
        private System.Windows.Forms.Button buttonNextGuess;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Label labelShowHint;
        private System.Windows.Forms.Label labelHintResult;
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.Label labelStats;
    }
}

