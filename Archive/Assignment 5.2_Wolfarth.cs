﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment_5._2_Wolfarth
{

    class Patient : IComparable
    {
        //Create a class named Patient that has the following data members: 
        //patient number(int), use a full accessor implementation, not automatic accessors.
       //patient name(string), use a full accessor implementation, not automatic accessors.
      //patient age(int), use a full accessor implementation, not automatic accessors.
     //amount due(double) from patient, use a full accessor implementation, not automatic accessors.
        private int patientNumber;
        public int PatientNumber
        {
            get
            {
                return patientNumber;

            }
            set
            {
                patientNumber = value;
            }
        }
        private string patientName;
        public string PatientName
        {
            get
            {
                return patientName;
            }
            set
            {
                patientName = value;
            }
        }
        private int patientAge;
        public int PatientAge
        {
            get
            {
                return patientAge;
            }
            set
            {
                patientAge = value;
            }
        }
        private double patientDues;
        public double PatientDues
        {
            get
            { return patientDues; }
            set
            { patientDues = value; }
        }
        //A class constructor that assigns default values of patient number = 9, name = "ZZZ", patient age = 0, patient amount due = 0.
        // The default values will be implemented through the default constructor by referencing the this pointer when the no argument 
        //constructor is implemented when assigning a patient object to the object array in Main.
        public Patient()
        {
            this.PatientNumber = 9;
            this.PatientName = "ZZZ";
            this.PatientAge = 0;
            this.PatientDues = 0;
        }
        //**A class constructor that takes four parameters to assign input values by way of the accessors for patient number, name, age, amount due.
        public Patient(int PatientNumber, string PatientName, int PatientAge, double PatientDues)
        { }
    }
    class Assignment
    {
        static void Main()
        {
            //instantiate an array of five (5) Patient objects.
            Patient[] info = new Patient[5];

            //**Implement a for-loop that will instantiate each Patient object in turn and assign it to an array element.
           // **prompt the user to enter a patient number.
           //**Implement a for-loop that will use the Patient object Equals method to compare the Patient objects in the array 
           //**and determine if the patient number entered is a duplicate.  
           //**If it is a duplicate then implement a while loop that will prompt the user that the id entered is a duplicate and
          // **to reenter a valid patient id; then check again using Equals to compare the number entered to the other patient object id numbers entered in the array.
         //**once a valid id number is entered then request entries for the name, age and amount due for the current Patient object.
         //**Once all five Patient array objects have been entered you will sort the array of objects by patient id number.

            //Write an output line that will give the header "Payment Information"
            Console.WriteLine("\nPayment Information");

            //implement a public static GetPaymentAmount method that will be called from Main.
            // The GetPaymentAmount method will be passed the this pointer for the current Patient object from the object array.
            //set a constant for the number of quarters in a year equal to 4.
            //return to the method call(assign to payment variable in Main) the Patient object amount due divided by the constant number of quarters.
            public static GetPaymentAmount
            {
                int const quarters= 4;
                return quaterpayment= PatientDues/quarters;
            }
            //**Implement a for-loop that will call the static GetPaymentAmount method passing the each (this pointer) object in the Patient object array.
            //**assign the current payment value returned from the GetPaymentAmount static method to an output variable declared in Main.
            //**use a console write to implement the overridden Patient object ToString method(see output example).
            //**use a write line to display "Quarterly payment is " followed by the payment extracted from the current object payment due.

        }
    }
}
