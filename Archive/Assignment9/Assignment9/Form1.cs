﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment9
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            dateTimePicker1.MinDate = DateTime.Today;
        }

        private void buttonSelectionComplete_Click(object sender, EventArgs e)
        {
            int days = 1 + (dateTimePicker1.Value - DateTime.Today).Days;
            labelDays.Text = days.ToString();
            var carStyle = listBoxCarStyle.SelectedItem.ToString();
            double totalPrice = 0;
            if (carStyle.Equals("Compact"))
            {
                totalPrice = days * 19.95;
            }
            if (carStyle.Equals("Standard"))
            {
                totalPrice = days * 24.95;
            }
            if (carStyle.Equals("Luxury"))
            {
                totalPrice = days * 39;
            }
            labelTotalPrice.Text = totalPrice.ToString("c2");
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
