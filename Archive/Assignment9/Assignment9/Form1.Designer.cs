﻿namespace Assignment9
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxCarStyle = new System.Windows.Forms.ListBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.buttonSelectionComplete = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.labelDays = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelTotalPrice = new System.Windows.Forms.Label();
            this.buttonExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBoxCarStyle
            // 
            this.listBoxCarStyle.FormattingEnabled = true;
            this.listBoxCarStyle.ItemHeight = 31;
            this.listBoxCarStyle.Items.AddRange(new object[] {
            "Compact",
            "Standard",
            "Luxury"});
            this.listBoxCarStyle.Location = new System.Drawing.Point(68, 40);
            this.listBoxCarStyle.Name = "listBoxCarStyle";
            this.listBoxCarStyle.Size = new System.Drawing.Size(149, 97);
            this.listBoxCarStyle.TabIndex = 0;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(68, 183);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(475, 38);
            this.dateTimePicker1.TabIndex = 1;
            // 
            // buttonSelectionComplete
            // 
            this.buttonSelectionComplete.BackColor = System.Drawing.Color.LightCyan;
            this.buttonSelectionComplete.Location = new System.Drawing.Point(68, 312);
            this.buttonSelectionComplete.Name = "buttonSelectionComplete";
            this.buttonSelectionComplete.Size = new System.Drawing.Size(475, 47);
            this.buttonSelectionComplete.TabIndex = 2;
            this.buttonSelectionComplete.Text = "Selection Complete";
            this.buttonSelectionComplete.UseVisualStyleBackColor = false;
            this.buttonSelectionComplete.Click += new System.EventHandler(this.buttonSelectionComplete_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(68, 378);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(322, 32);
            this.label1.TabIndex = 3;
            this.label1.Text = "Number of Rental Days: ";
            // 
            // labelDays
            // 
            this.labelDays.AutoSize = true;
            this.labelDays.Location = new System.Drawing.Point(414, 378);
            this.labelDays.Name = "labelDays";
            this.labelDays.Size = new System.Drawing.Size(0, 32);
            this.labelDays.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(68, 423);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(194, 32);
            this.label2.TabIndex = 5;
            this.label2.Text = "Total Price is: ";
            // 
            // labelTotalPrice
            // 
            this.labelTotalPrice.AutoSize = true;
            this.labelTotalPrice.Location = new System.Drawing.Point(446, 422);
            this.labelTotalPrice.Name = "labelTotalPrice";
            this.labelTotalPrice.Size = new System.Drawing.Size(0, 32);
            this.labelTotalPrice.TabIndex = 6;
            // 
            // buttonExit
            // 
            this.buttonExit.BackColor = System.Drawing.Color.LightCyan;
            this.buttonExit.Location = new System.Drawing.Point(74, 495);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(188, 51);
            this.buttonExit.TabIndex = 7;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = false;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(942, 602);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.labelTotalPrice);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelDays);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonSelectionComplete);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.listBoxCarStyle);
            this.Name = "Form1";
            this.Text = "Car Rental";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxCarStyle;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button buttonSelectionComplete;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelDays;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelTotalPrice;
        private System.Windows.Forms.Button buttonExit;
    }
}

