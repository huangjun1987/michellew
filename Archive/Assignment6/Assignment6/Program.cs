﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment6
{
    class Program
    {
        public static CreditCustomer[] creditCustomers = new CreditCustomer[2];

        static void Main(string[] args)
        {
            for (int i = 0; i < 5; i++)
            {
                Console.Write("Enter customer number ");
                var creditCustomer = EnterCustomer();
                creditCustomers[i] = creditCustomer;
            }
            Array.Sort(creditCustomers);
            Console.WriteLine("Summary:");
            double totalAmountDue = 0;
            for (int i = 0; i < creditCustomers.Length; i++)
            {
                Console.WriteLine(creditCustomers[i].ToString());
                totalAmountDue += creditCustomers[i].AmountDue;
            }
            Console.WriteLine($"AmountDue for all Customers is : {totalAmountDue}");
            Console.WriteLine("Payment Information:");
            for (int i = 0; i < creditCustomers.Length; i++)
            {
                Console.WriteLine(creditCustomers[i].ToString());
                Console.WriteLine($"Monthly payment is  {creditCustomers[i].GetPaymentAmounts():c2}");
            }
            Console.ReadLine();
        }

        private static CreditCustomer EnterCustomer()
        {
            var number = int.Parse(Console.ReadLine());
            for (int i = 0; i < creditCustomers.Length; i++)
            {
                if (creditCustomers[i] != null && creditCustomers[i].Equals(number))
                {
                    Console.WriteLine($"Sorry, the customer number {number} is a duplicate.");
                    Console.Write("Enter reenter ");
                    return EnterCustomer();
                }
            }
            Console.Write("Enter name ");
            var name = Console.ReadLine();
         
            Console.Write("Enter amount due ");
            var amountDue = double.Parse(Console.ReadLine());

            Console.Write("Enter interest rate ");
            var rate = double.Parse(Console.ReadLine());

            return new CreditCustomer(number, name, amountDue, rate);

        }
    }
}
