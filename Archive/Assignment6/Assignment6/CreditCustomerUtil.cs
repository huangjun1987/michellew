﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment6
{
    public static class CreditCustomerUtil
    {
        public static double GetPaymentAmounts(this CreditCustomer creditCustomer)
        {
            return creditCustomer.AmountDue / 24;

        }
    }
}
