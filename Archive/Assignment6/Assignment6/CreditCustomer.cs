﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment6
{
   public class CreditCustomer: Customer, IComparable
    {
        public double Rate { get; set; }

        public CreditCustomer(int number, string name, double amountDue, double rate): base(number, name, amountDue)
        {
            Rate = rate;
            base.c(number, name, amountDue);
        }
        public CreditCustomer():this(0, "", 0, 0)
        {

        }
        
        public override string ToString()
        {
            return $"{GetType().Name} {Number} {Name} AmountDue is {AmountDue:c2} Interest rate is {Rate}";
        }
    }
}
