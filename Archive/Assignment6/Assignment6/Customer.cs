﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment6
{
    public class Customer: IComparable
    {
        public Customer(int number, string name, double amountDue)
        {
            Number = number;
            Name = name;
            AmountDue = amountDue;
        }
        public Customer(): this(9, "ZZZ", 0)
        {
            
        }
        public int Number { get; set; }
        public string Name { get; set; }
        public double AmountDue { get; set; }

        public int CompareTo(object obj)
        {
            return Number.CompareTo(((Customer)obj).Number);
        }
        public override bool Equals(object obj)
        {
            return Number.Equals(obj);
        }

        public override int GetHashCode()
        {
            return Number;
        }

        public override string ToString()
        {
            return $"{GetType().Name} {Number} {Name} AmountDue is {AmountDue:c2}";
        }
    }
}
